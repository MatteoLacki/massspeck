R_install:
	R CMD build MassSpeckR
	R CMD INSTALL MassSpeckR_0.1.0.tar.gz
Py_install:
	pip2 install -e MassSpeckPy/.
	pip3 install -e MassSpeckPy/.
clean:
	rm *.tar.gz
	R CMD REMOVE MassSpeckR